# NDM Cloudflare

## Manage records in Cloudflare sites


### DOWNLOAD ###

You can clone/download the last version of the full project or just only the main script:
- clone with SSH: `git clone git@gitlab.com:nidamanx/ndm-cloudflare.git`
- [clone full project with https](https://gitlab.com/nidamanx/ndm-cloudflare.git)


### INSTALL ###

- Save this script (or an alias of it) in an appropriate directory (eg: `/usr/local/bin/` or `~/.local/bin/` or `~/bin/`)
- Change script permission as follows: `chmod +x ndm_*.sh`
- Prepare secret file: `cp -a env.txt .env`
- Add your secrets: `editor .env`
- Protect .env file: `sudo chown root:root .env && sudo chmod 600 .env`
- Create cron: `sudo crontab -e`

Suggested steps:
1. `sudo apt install -y git`
2. `mkdir ~/bin`
3. `cd ~/bin`
4. `git clone git@gitlab.com:nidamanx/ndm-cloudflare.git`
5. `cd ndm-cloudflare`
5. `chmod +x *.sh`
6. `cp -a env.txt .env`
7. `editor .env`
8. `sudo chown root:root .env && sudo chmod 600 .env`
9. `sudo ln -s ~/bin/ndm-cloudflare/ndm_cloudflare.sh /usr/local/bin/ndm_cloudflare`
10. `sudo ln -s ~/bin/ndm-cloudflare/cron.sh /usr/local/bin/ndm_cloudflare-cron`


### CONFIGURE ###

Configure the parameters in ndm_cloudflare.sh appropriare section


### CRON ###

`sudo crontab -e`

_set crontab as the following schema_

```
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of the month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12)
# │ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday 7 is also Sunday on some systems)
# │ │ │ │ │ ┌───────────── command to issue                               
# │ │ │ │ │ │
# │ │ │ │ │ │
# * * * * * /bin/bash {Location of the script}

# example: execute every 12 minutes
*/12 * * * * /bin/bash /usr/local/bin/ndm_cloudflare-cron
```

### USAGE ###

This script needs root privileges

`sudo ndm_cloudflare` or `su -c 'ndm_cloudflare'`


### UPDATE ###
1. `cd ~/bin/ndm-cloudflare/`
2. `git pull`


### HELP ###

This script needs root privileges

`ndm_cloudflare -h` or `ndm_cloudflare --help'`


### LICENSE and DISCLAIMER ###

Copyright (C) 2020-2035 by Nicola Davide Mannarelli (@nidamanx) - NDMnet. All Rights Reserved.

This code is licensed under GPLv3 license (see LICENSE for details)
The above copyright notice and this permission notice shall be
included and visible in the top of all copies or portions of this
Software.

This program is free software.
You can use, redistribute and/or modify it under the terms of the
GNU General Public License published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Author, Owner, Maintainer and Contributors are not responsable for
any damage or data loss. Use at your own risk.

---
---


#### NOTES ####

- This software if fully tested with Debian 11 Bullseye

- Code check is done with `shellcheck` and `checkbashisms`
