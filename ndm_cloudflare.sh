#!/usr/bin/env bash
#
# NDM Cloudflare
# Manage records in Cloudflare sites
#

readonly K_VERSION='1.0.0'

: '
Copyright (C) 2020-2035 by Nicola Davide Mannarelli (@nidamanx) - NDMnet. All Rights Reserved.

This code is licensed under GPLv3 license (see LICENSE for details)
The above copyright notice and this permission notice shall be
included and visible in the top of all copies or portions of this
Software.

This program is free software.
You can use, redistribute and/or modify it under the terms of the
GNU General Public License published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Author, Owner, Maintainer and Contributors are not responsable for
any damage or data loss. Use at your own risk.
'

### DESCRIPTION ###
# Manage records in Cloudflare sites
### INSTALL ###
# 1. Save this script on your computer (eg: `/usr/local/bin/` `~/.local/bin/` or `~/bin/`)
# 2. Change permission as follows: `chmod +x ndm_cloudflare`
### CONFIGURE ###
# Configure the parameters in the appropriare section
### USAGE ###
# `sudo ndm_cloudflare` or `su -c 'ndm_cloudflare'`
### LICENSE ###
# Copyright (C) 2020-2035 by Nicola Davide Mannarelli (@nidamanx) - NDMnet. All Rights Reserved.
# This sowtware is licensed under GPLv3 license.


##################################################################
# START EDITING BELOW THIS LINE                                  #
##################################################################


########################
# CONFIGURE
########################


# Set to "global" for Global API Keys or "token" for Custom API Tokens
v_auth_method="token"
# Set the DNS TTL (default 300 seconds)
v_ttl="300"
# Set the proxy to true or false
v_proxy=false


##################################################################
# DO NOT EDIT BELOW THIS LINE UNLESS YOU KNOW WHAT YOU ARE DOING #
##################################################################

# Get current file path
K_REALPATH=$(realpath "$0")
readonly K_REALPATH
K_DIRNAME=$(dirname "$K_REALPATH")
readonly K_DIRNAME

# Import secrets from .env file
if [ -f "$K_DIRNAME/.env" ]; then
  source "$K_DIRNAME/.env"
else
  printf "\n.env file not found\n\n"
  exit 1
fi

# Log prefix
readonly v_log_prefix="ndm_cloudflare:"

# Set vars as readonly to be safe
readonly v_auth_email
readonly v_auth_method
readonly v_auth_key
readonly v_zone_id
readonly v_record_name
readonly v_ttl
readonly v_proxy


########################
# FUNCTIONS
########################


# Print usage
function f_usage() {
  local v_usage=\
"
Usage: sudo ndm_cloudflare [-w[-q -l]] | [-h|-u|-v]
"
  printf "%s\n" "${v_usage}"
}

# Print help
function f_help() {
  local v_help=\
"
Usage: sudo ndm_cloudflare [OPTION]
NDM Cloudflare manage records in Cloudflare sites

Examples:
  ndm_cloudflare --write
  ndm_cloudflare --write --quiet
  ndm_cloudflare --write --quiet --log
  ndm_cloudflare -wql

 Main operation mode:
  -w, --write      update IP in A record

 Operation modifiers:
  -q, --quiet      don't ask questions and don't show any progress
  -l, --log        write system logs

 Other options:
  -h, --help       show this help
  -u, --usage      show a short usage message
  -v, --version    show software version
"
  printf "%s\n" "${v_help}"
}

# Check if all commands are available
function f_check_command() {
  command -v "${1}" >/dev/null 2>&1 || {
    printf "\nCommand '%s' is required but not installed.\n\n" "${1}" >&2
    exit 1
  }
}

# Check confirmations
function f_ask_confirmation() {
  while true; do
    printf "\n%s" "${1}"
    read -r v_choice
    case "${v_choice}" in
      Y|Yes )
        printf "OK, preparing...\n\n"
        break
        ;;
      N|No )
        printf "Nothing is done\n"
        break
        ;;
      C|Cancel )
        printf "Script aborted\n\n"
        exit 0
        ;;
      * )
        printf "Invalid answer (check uppercase)\n\n" >&2
        ;;
    esac
  done
}

# Show message
function f_print_msg() {
  if [[ "${v_quiet}" == false ]]; then
    case "${#}" in
      1 )
        printf "%s" "${1}"
        ;;
      2 )
        printf "%s\n" "${1}"
        ;;
      3 )
        printf "\n%s\n" "${2}"
        ;;
      * )
        printf ""
        ;;
    esac
  fi
}

# Print version
function f_version() {
  local v_author='Nicola Davide Mannarelli - NDMnet'
  local v_name='NDM Cloudflare'
  printf "\n%s %s\n%s\n\n" "${v_name}" "${K_VERSION}" "${v_author}"
}


########################
### MAIN             ###
########################

function main() {

  ### BASIC CHECKS ###  
  
  # Check SuperUser
  if [[ "${USER}" != 'root' ]]; then
    printf "\nThis script needs SuperUser privileges\n\n" >&2
    exit 1
  fi
  
  # Check needed commands
  readonly a_needed_commands=( "curl" "sed" "logger" "cron" )
  for i in "${a_needed_commands[@]}"; do
    f_check_command "${i}"
  done
  
  # Ask for confirmation to use this script
  if [[ "${v_quiet}" == false ]]; then
    v_msg="Do you really want to execute this script? ([Y]es/[N]o/[C]ancel)? "
    f_ask_confirmation "${v_msg}"
  fi
  
  # Check for mandatory empty vars
  if [[ 
      ( -z "$v_auth_email" ) || 
      ( -z "$v_auth_method" ) ||
      ( -z "$v_auth_key" ) ||
      ( -z "$v_zone_id" ) ||
      ( -z "$v_record_name" ) ||
      ( -z "$v_ttl" ) ||
      ( -z "$v_proxy" )
  ]]; then
    printf "\nOne or more variables are not set\nPlease, check .env file\n\n" >&2
    exit 1
  fi

  
  ### START MAIN PROCESS ###


  # Get public IP
  ip=$(curl -s https://api.ipify.org || curl -s https://ipv4.icanhazip.com/)
  if [[ "${ip}" == "" ]]; then
    v_msg="$v_log_prefix No public IP found"
    f_print_msg "\n" "${v_msg}" "\n"
    if [[ "${v_log}" == true ]]; then
      logger -s "${v_msg}"
    fi
    exit 1
  fi
  
  
  # Check and set auth header
  if [[ "${v_auth_method}" == "global" ]]; then
    auth_header="X-Auth-Key:"
  elif [[ "${v_auth_method}" == "token" ]]; then
    auth_header="Authorization: Bearer"
  fi
  
 
  # Init Cloudflare communication
  v_msg="Init Cloudflare communication"
  f_print_msg "${v_msg}" "\n"
  if [[ "${v_log}" == true ]]; then
    logger "$v_log_prefix ${v_msg}"
  fi
  record=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$v_zone_id/dns_records?type=A&name=$v_record_name" \
    -H "X-Auth-Email: $v_auth_email" \
    -H "$auth_header $v_auth_key" \
    -H "Content-Type: application/json")
  

  # Check domain for record "A"
  if [[ $record == *"\"count\":0"* ]]; then
    v_msg="Record does not exist. Please, create one (${ip} for ${v_record_name})"
    f_print_msg "${v_msg}" "\n"
    if [[ "${v_log}" == true ]]; then
      logger -s "$v_log_prefix ${v_msg}"
    fi
    exit 1
  fi
  

  # Get current IP
  old_ip=$(echo "$record" | sed -E 's/.*"content":"(([0-9]{1,3}\.){3}[0-9]{1,3})".*/\1/')
  if [[ $ip == "$old_ip" ]]; then
    v_msg="IP ($ip) for ${v_record_name} has not changed"
    f_print_msg "${v_msg}" "\n"
    if [[ "${v_log}" == true ]]; then
      logger "$v_log_prefix ${v_msg}"
    fi
    exit 0
  fi
  
  
  # Set the record identifier from result
  record_identifier=$(echo "$record" | sed -E 's/.*"id":"(\w+)".*/\1/')
  

  # Update the IP using the API
  update=$(curl -s -X PATCH "https://api.cloudflare.com/client/v4/zones/$v_zone_id/dns_records/$record_identifier" \
    -H "X-Auth-Email: $v_auth_email" \
    -H "$auth_header $v_auth_key" \
    -H "Content-Type: application/json" \
    --data "{\"type\":\"A\",\"name\":\"$v_record_name\",\"content\":\"$ip\",\"v_ttl\":\"$v_ttl\",\"proxied\":${v_proxy}}")
  

  # Report the status
  case "$update" in
  *"\"success\":false"*)
    v_msg="$ip $v_record_name failed for $record_identifier ($ip). RESULTS:\n$update"
    f_print_msg "${v_msg}" "\n"
    if [[ "${v_log}" == true ]]; then
      logger -s "$v_log_prefix ${v_msg}"
    fi
    exit 1
    ;;
  *)
    v_msg="$v_record_name updated to $ip"
    f_print_msg "${v_msg}" "\n"
    # log always all updates
    logger "$v_log_prefix ${v_msg}"
    ;;
  esac

  
  # All Done
  f_print_msg "\n" "All done" "\n"
  f_print_msg "" "\n"
  exit 0
}


########################
# MANAGE ARGUMENTS
########################


v_arguments=$(getopt -q -n "${0##*/}" \
  -o 'huvwql' \
  -l 'help,usage,version,write,quiet,log' \
  -- "${@}")
v_valid_arguments="${?}"
if [ "${v_valid_arguments}" != "0" ]; then
  f_help
  exit 1
fi
if [[ "${#}" -lt 1 ]]; then
  f_help
else
  v_write=false
  v_quiet=false
  v_log=false
  eval set -- "${v_arguments}"
  while true; do
    case "${1}" in
      -h|--help)
        f_help
        break
        ;;
      -q|--quiet)
        v_quiet=true
        shift
        ;;
      -l|--log)
        v_log=true
        shift
        ;;
      -w|--write)
        v_write=true
        shift
        ;;
      -u|--usage)
        f_usage
        break
        ;;
      -v|--version)
        f_version
        break
        ;;
      --)
        shift
        break
        ;;
      *)
        f_help
        break
        ;;
    esac
  done
  if [[ "${v_write}" == true ]]; then
    main
  fi
fi

exit 0
