#!/usr/bin/env bash

K_REALPATH=$(realpath "$0")
readonly K_REALPATH
K_DIRNAME=$(dirname "$K_REALPATH")
readonly K_DIRNAME

v_ndm_cloudflare_path="$K_DIRNAME/ndm_cloudflare.sh"

if [[ -f "${v_ndm_cloudflare_path}" ]]; then
    eval "sudo ${v_ndm_cloudflare_path} -wq"
  else
    eval "sudo ndm_cloudflare -wq"
fi

exit 0
